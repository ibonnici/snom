// Considering that either "qseq sseq" and/or "btop" columns
// are used by the process, but that the columns ordering must remain strict
// to avoid useless backtracking and heap allocations on every line,
// the logic around columns handling is not quite straightforward.
// Gather most of it here.

use std::array;

use arrayvec::ArrayVec;

use crate::{
    config::{ColumnsNames, ColumnsUsed},
    errors::{errout, Error},
};

// Max number of used colums.
pub(crate) const MAX_N_COLS: usize = 6; // id qstart qend btop qseq sseq

// The useful information about columns
// is the offset from every column to the next,
// so that the search can tell how many columns to skip
// to find the next interesting field on every line.
// The actual number of skips depends on the columns used.
#[cfg_attr(test, derive(Debug, PartialEq))]
pub(crate) struct ColumnsSkips {
    // The first skips are fixed.
    pub(crate) id: usize,
    pub(crate) query_start: usize,
    pub(crate) query_end: usize,
    // The other depend on the columns contained in the input.
    pub(crate) alignment: AlignmentSkips,
}

#[cfg_attr(test, derive(Debug, PartialEq))]
pub(crate) enum AlignmentSkips {
    Btop([usize; 1]),
    MatchSequences([usize; 2]),
    BtopThenSequences([usize; 3]),
    SequencesThenBtop([usize; 3]),
}

impl ColumnsUsed {
    // Identify the columns used based on the names at hand.
    // None if no order matched.
    pub(crate) fn new<'i, I>(at_hand: &I, colnames: &ColumnsNames) -> Option<Self>
    where
        I: Iterator<Item = &'i str> + Clone,
    {
        // Scroll candidates orders until a match is found.
        for c in Self::iter() {
            let expected = c.expected_order(colnames);
            if expected.into_iter().eq(at_hand.clone()) {
                return Some(c);
            }
        }
        None
    }
}

// When no header is provided, parse skips from the command line arguments.
pub(crate) fn parse_column_skips(input: &str, used: ColumnsUsed) -> Result<ColumnsSkips, Error> {
    use Error as E;

    macro_rules! err {
        ($fmt:tt) => {
            E::Arguments(format!$fmt)
        };
        (:out $fmt:tt) => {
            return Err(err!($fmt));
        };
    }

    // Collect numbers regardless of the column names.
    let n_cols = used.len();
    let mut skips = ArrayVec::<_, MAX_N_COLS>::new();
    let mut last_i = 0;
    for i in input.split('-') {
        let i = i.parse::<usize>().map_err(|e| {
            if input.contains('-') {
                err!(("Cannot parse {i:?} as a column number: {e}"))
            } else {
                err!(("Separator '-' not found in column numbers specification {input:?}."))
            }
        })?;
        if i <= last_i {
            errout!(Arguments(format!(
                "Column number {i} is not superior to predecessor ({last_i}). \
                 Columns numbers must be strictly increasing for performance reasons."
            )));
        }
        let skip = i - last_i - 1;
        if skips.len() == n_cols {
            err!(:out("Too many column numbers received. Expected {n_cols}."));
        }
        skips.push(skip);
        last_i = i;
    }
    let n = skips.len();
    if n < n_cols {
        err!(:out("Expected {n_cols} column numbers, received only {n}."));
    }
    // Assign indexes wrt the order chosen.
    Ok(ColumnsSkips::new(used, skips.into_iter()))
}

impl ColumnsSkips {
    // Construct from iterator,
    // assuming the exact required number of values is yielded.
    // Panic otherwise.
    pub(crate) fn new(used: ColumnsUsed, mut skips: impl Iterator<Item = usize>) -> Self {
        let mut next = || skips.next().expect("Missing skip values");
        macro_rules! map {
            ($($used:ident => $skips:ident)+) => {
                match used {
                $(
                    ColumnsUsed::$used => AlignmentSkips::$skips(array::from_fn(|_| next())),
                )+
                }
            };
        }
        let res = ColumnsSkips {
            id: next(),
            query_start: next(),
            query_end: next(),
            alignment: map! {
                IdQstartQendBtop => Btop
                IdQstartQendQseqSseq => MatchSequences
                IdQstartQendBtopQseqSseq => BtopThenSequences
                IdQstartQendQseqSseqBtop => SequencesThenBtop
            },
        };
        assert!(skips.next().is_none(), "Too many skip values.");
        res
    }
}

// Various trivial requests regarding the ordering used.
impl ColumnsUsed {
    // Hom many columns used.
    pub(crate) fn len(self) -> usize {
        use ColumnsUsed as C;
        match self {
            C::IdQstartQendBtop => 4,
            C::IdQstartQendQseqSseq => 5,
            C::IdQstartQendQseqSseqBtop | C::IdQstartQendBtopQseqSseq => 6,
        }
    }

    // Expected columns names ordering.
    pub(crate) fn expected_order(self, names: &ColumnsNames) -> ArrayVec<&str, MAX_N_COLS> {
        use ColumnsUsed as C;
        let mut res = ArrayVec::new();
        macro_rules! push {
            ($($col:ident)+) => {
                // Fixed part.
                res.push(names.id.as_str());
                res.push(names.query_start.as_str());
                res.push(names.query_end.as_str());
                // Variable part.
                $( res.push(names.$col.as_str()); )+
            };
        }
        match self {
            C::IdQstartQendBtop => {
                push!(btop);
            }
            C::IdQstartQendQseqSseq => {
                push!(query_sequence subject_sequence);
            }
            C::IdQstartQendQseqSseqBtop => {
                push!(query_sequence subject_sequence btop);
            }
            C::IdQstartQendBtopQseqSseq => {
                push!(btop query_sequence subject_sequence);
            }
        }
        res
    }

    // List all possible values.
    pub(crate) fn iter() -> impl Iterator<Item = Self> {
        use ColumnsUsed as C;
        [
            C::IdQstartQendBtop,
            C::IdQstartQendQseqSseq,
            C::IdQstartQendQseqSseqBtop,
            C::IdQstartQendBtopQseqSseq,
        ]
        .into_iter()
    }
}
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse_raw_skips() {
        use ColumnsUsed as C;
        use Error as E;

        let used = C::IdQstartQendQseqSseq;

        let success = |input, expected: [_; 5]| {
            println!("Expect success when parsing skips:\n  {input:?}");
            let actual = parse_column_skips(input, used).unwrap();
            let expected = ColumnsSkips::new(used, expected.into_iter());
            assert_eq!(expected, actual);
        };

        // Simplest file.
        success("1-2-3-4-5", [0, 0, 0, 0, 0]);
        // Doc example.
        success("1-9-10-12-13", [0, 7, 0, 1, 0]);

        let failure = |input, message| {
            println!("Expect failure when parsing skips:\n {input:?}");
            match parse_column_skips(input, used) {
                Ok(skips) => panic!("Unexpected success: yielded {skips:?}."),
                Err(E::Arguments(actual)) => assert_eq!(message, actual),
                Err(e) => panic!("Unexpected error type: {e:?}."),
            };
        };

        // Invalid separator.
        failure(
            "1 2 3 4 5",
            "Separator '-' not found in column numbers specification \"1 2 3 4 5\".",
        );
        failure(
            "",
            "Separator '-' not found in column numbers specification \"\".",
        );
        // Invalid integers.
        failure(
            "a-b-c-d-e",
            "Cannot parse \"a\" as a column number: invalid digit found in string",
        );
        // Not enough values.
        failure("1-2", "Expected 5 column numbers, received only 2.");
        // To many values.
        failure(
            "1-2-3-4-5-6",
            "Too many column numbers received. Expected 5.",
        );
        // Out-of order.
        failure(
            "1-3-2-4-5",
            "Column number 2 is not superior to predecessor (3). \
             Columns numbers must be strictly increasing for performance reasons.",
        );
        failure(
            "1-2-2-4-5",
            "Column number 2 is not superior to predecessor (2). \
             Columns numbers must be strictly increasing for performance reasons.",
        );
        failure(
            "1-9-12-10-13",
            "Column number 10 is not superior to predecessor (12). \
             Columns numbers must be strictly increasing for performance reasons.",
        );
    }
}
