// Core algorithm version with sequence alignments:
// given query/subject sequences with indels,
// find bases in homologous position in original query coordinates.
// Assumptions at this point:
//   - query/subject have the same length.
//   - the original coordinate of the base of interest
//     does fall within the query sequences.

// TODO: receive NonZeroU64 input?
// TODO: leave `blast_start` handling up to the caller? (or would it make errors less readable?)

use crate::errors::{errout, Error};

// The idea is to scroll the query side of the blast,
// counting from the start up to the base of interest,
// but skipping deletions (-) in the query match.
// In practice:
//  - `offset` is the position of the focal character
//    (including indels, counting from 0).
//  - `focus` keeps track of the original coordinate of the base being read.
//    (excluding indels, counting from 1).
// Increase both consistently until `focus` matches `base_of_interest`.
pub(crate) fn search_match_sequences(
    query: &[u8],          // Not empty.
    subject: &[u8],        // Not empty.
    base_of_interest: u64, // Coordinate in query sequence: counting from 1.
    blast_start: u64,      // Coordinate of the alignment start: counting from 1.
    n_line: usize,
) -> Result<(u8, u8), Error> {
    use Error as E;

    let (offset, q) = {
        let mut focus = blast_start;
        let mut offset = 0;
        let mut it = query.iter().copied();
        loop {
            if let Some(q) = it.next() {
                if q != b'-' {
                    focus += 1;
                }
                if focus == base_of_interest + 1 {
                    break (offset, q);
                }
            } else {
                errout!(Search(
                    n_line,
                    "Query sequence shorter than expected.".into()
                ));
            }
            // Go read next char.
            offset += 1;
        }
    };

    // The offset is the coordinate of homologous base in the matched sequence.
    let s = *subject.get(offset).ok_or(E::Search(
        n_line,
        "Subject sequence shorter than expected.".into(),
    ))?;

    // So we've got the two homologous bases.
    Ok((q, s))
}

// A few simple tests to check that we agree on the logic here.
#[test]
fn short_homologies() {
    macro_rules! tests {
        ($($name:ident: $start:literal $coord:literal $(v)?
           $query:literal $subject:literal -> $expected:expr)+) => {
            $(
                let (start, coord, query, subject) = ($start, $coord, $query, $subject);
                println!("Checking ({start}, {coord}) in:\n  {query}\n  {subject}");
                let (q, s) = search_match_sequences(
                    $query.as_bytes(),
                    $subject.as_bytes(),
                    $coord,
                    $start,
                    0,
                ).unwrap();
                assert_eq!($expected, (q as char, s as char), "check id: {}", stringify!($name));
            )+
        };
    }
    tests! {

    simple:
        1   5                    // (blast start)  (original coordinate)
       "AAAATGGGG"               // (query side of the blast)
       "AAAACGGGG" -> ('T', 'C') // (subject side of the blast) (result)

    simple_shift:
        1  4
       "AAATGGGGG"
       "AAACGGGGG" -> ('T', 'C')

    degenerated_left:
        1
        1
       "TGGGGGGGG"
       "CGGGGGGGG" -> ('T', 'C')

    degenerated_in:
        5
        5
       "TGGGG"
       "CGGGG" -> ('T', 'C')

    degenerated_right:
        9
        9
       "T"
       "C" -> ('T', 'C')

    // With the blast start not matching query start.
    blast_start:
       95   100
             v
       "12345TAAAAA"
       "12345CAAAAA" -> ('T', 'C')

    first_base:
       95
       95
        v
       "12345TAAAAA"
       "12345CAAAAA" -> ('1', '1')

    last_base:
       95        105
                  v
       "12345TAAAAX"
       "12345CAAAAX" -> ('X', 'X')

    // With deletions.
    deletions_1:
        1    6
       "12345TAAAAA"
       "12-45CAAAAA" -> ('T', 'C')

    deletions_2:
        1    6
       "12345TAAAAA"
       "12-4-CAAAAA" -> ('T', 'C')

    deletions_3:
        1    6
       "12345TAAAAA"
       "12-4--AAAAA" -> ('T', '-')

    deletions_first:
        1    6
       "12345TAAAAA"
       "-2-4--AAAAA" -> ('T', '-')

    // With insertions.
    insertions_1:
        1     6
       "1-2345TAAAA"
       "1X2345CAAAA" -> ('T', 'C')

    insertions_2:
        1       6
       "1-23--45TAA"
       "1X23XX45CAA" -> ('T', 'C')

    insertions_3:
        1        6
       "1-23--45-TAA"
       "1X23XX45XCAA" -> ('T', 'C')

    insertions_first:
        1         6
       "-1-23--45-TA"
       "X1X23XX45XCA" -> ('T', 'C')

    // With both.
    indels_1:
        1       6
       "1-23--45TAA"
       "1X2-XX4-CAA" -> ('T', 'C')

    full_fledged:
        95     100
                v
       "1-23--45TAA"
       "1X2-XX4-CAA" -> ('T', 'C')

    };
}
