use std::{
    fs::File,
    io::{self, BufRead, BufReader, Read, Write},
    process,
};

use ansi_term::Colour;
use clap::Parser;
use errors::Error;
use num_format::{Locale, ToFormattedString};

use crate::{config::Config, parse_header::parse_header, process_line::process_line};

mod columns;
mod config;
mod errors;
mod parse_header;
mod process_line;
mod search_btop;
mod search_match_sequences;

pub const VERSION: &str = env!("CARGO_PKG_VERSION");
pub const NAME: &str = env!("CARGO_PKG_NAME");

fn main() {
    eprintln!("Running {NAME} v{VERSION}.");
    match run() {
        Ok(()) => {
            eprintln!("Success. {}", Colour::Green.bold().paint("✔"));
        }
        Err(e) => {
            eprintln!("{} {e}", Colour::Red.bold().paint("🗙"));
            process::exit(e.code());
        }
    }
}

fn run() -> Result<(), Error> {
    use Error as E;

    // Parse command-line arguments.
    let args = Config::parse();
    let input_separator = args.separator(true)?;
    let output_separator = args.separator(false)?;

    let path = &args.input;

    // Read from either stdin or a file.
    // With the "On-stack dynamic dispatch" pattern found there:
    // https://rust-unofficial.github.io/patterns/idioms/on-stack-dyn-dispatch.html
    let (mut stdin_read, mut file_read);
    let readable: &mut dyn Read = if path == "-" {
        eprintln!("Reading from standard input.");
        stdin_read = io::stdin();
        &mut stdin_read
    } else {
        eprintln!("Opening input file: {}", Colour::Blue.paint(path));
        file_read = File::open(path).map_err(E::InputIO)?;
        &mut file_read
    };
    let mut lines = LinesReader::new(readable);

    // Retrieve necessary column information.
    let colnames = args.column_names()?;
    let colskips = {
        if args.columns_positions.is_some() {
            eprintln!(
                "Assuming that there is no header line \
                 because explicit column positions have been provided."
            );
            let used = args.columns_used.unwrap(); // Checked by clap.
            let skips = args.column_skips(used).unwrap(); // Checked by clap.
            skips?
        } else {
            eprintln!("Reading header line..");
            let header = lines
                .next()?
                .ok_or_else(|| E::HeaderParse("Input file is empty.".into()))?
                .0;
            parse_header(header, colnames, args.columns_used, input_separator)?
        }
    };

    eprintln!("Searching homologous SNPs..");

    // Process every input line in a row.
    // Keep track of current line number to produce useful error messages.
    let mut n_line = 0;
    // Display and update a short report as it goes.
    let report = 50_2687; // Use a prime number to vary display.
    let mut body = || -> Result<(), Error> {
        while let Some((line, n)) = lines.next()? {
            n_line = n;
            if n_line % report == 0 {
                eprint!(
                    "\r  Processing line {}..",
                    n_line.to_formatted_string(&Locale::en)
                );
                io::stderr().flush().map_err(E::OutputIO)?;
            }

            let (q, s) = process_line(line, n_line, &colskips, input_separator, colnames)?;
            io::stdout()
                .write_all(&[q, output_separator, s, b'\n'])
                .map_err(E::OutputIO)?;
        }
        Ok(())
    };
    // Display possible error on their own line.
    body().map_err(|e| {
        eprintln!();
        e
    })?;
    eprintln!(
        "\r  Processing line {}..",
        n_line.to_formatted_string(&Locale::en)
    );

    Ok(())
}

// Parse file as a raw sequence of bytes to elide utf-8 checking.
// Only rely on '\n' line separators.
// Bytes will be copied from the file into the buffered reader,
// then into the line buffer.
// But the line buffer will only be allocated once.
struct LinesReader<'r> {
    reader: BufReader<&'r mut dyn Read>,
    line: Vec<u8>,
    n: usize,
}

impl<'r> LinesReader<'r> {
    fn new(reader: &'r mut dyn Read) -> Self {
        Self {
            reader: BufReader::new(reader),
            line: Vec::new(),
            n: 0,
        }
    }

    // Extract next line and its number.
    fn next(&mut self) -> Result<Option<(&[u8], usize)>, Error> {
        self.line.clear();
        self.reader
            .read_until(b'\n', &mut self.line)
            .map(|n| {
                if n > 0 {
                    self.n += 1;
                    Some((
                        self.line.strip_suffix(&[b'\n']).unwrap_or(&self.line),
                        self.n,
                    ))
                } else {
                    None
                }
            })
            .map_err(|e| Error::InputLineIO(self.n, e))
    }
}
