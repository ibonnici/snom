// Core algorithm version with btop alignment digest:
// The btop is supposed to have the following form:
//   <n>((<q><s>)+<n>)*
// With <n> a number of contiguous aligned nucleotides,
// <q> a mismatched nucleotide in the query and <s> the aligned nucleotide in the subject.
// This is enough information to figure the value of <q> and <s> at the position of interest
// in case they are mismatched because they would appear as <q><s> sequence,
// but not enough to figure their (equal) value in case they are matched
// because they would be elided within the <n> number.
// In the latter situation, output '=' symbols instead of regular [ATGC-] symbols.

use std::str;

use crate::errors::{errout, Error};

pub(crate) fn search_btop(
    btop: &[u8],           // Not empty.
    base_of_interest: u64, // Coordinate in query sequence: counting from 1.
    blast_start: u64,      // Coordinate of the alignment start: counting from 1.
    n_line: usize,
) -> Result<(u8, u8), Error> {
    use Error as E;
    use State as S;

    macro_rules! err {
        ($args:tt) => {
            errout!(Search(n_line, format!$args));
        };
    }

    // Same procedure as the search in match sequences:
    // update the 'focus' value as we read and stop when it reaches the 'target'.
    // Use a small formal automaton state.
    #[cfg_attr(test, derive(Debug))]
    enum State {
        N(usize), // Collecting digits, with index of first found.
        S,        // Expecting a subject char.
        Z,        // Expecting anything after subject char is read.
        // Having found the mismatched query position of interest,
        // and expecting the corresponding subject.
        Found(u8),
    }

    // Coordinate under focus in query sequence, counting from 1.
    let mut focus = blast_start - 1; // (so the initial position is before bound: nothing focused)
    let target = base_of_interest;
    let mut state = S::N(0);
    macro_rules! process_digits {
        ($i_skip:expr, $i:expr) => {{
            // A sequence of digits has just terminated. Read and parse.
            // SAFETY: only ascii chars have been collected within this slice,
            //         or the function would have errored out.
            let (i_skip, i) = ($i_skip, $i);
            let skip = unsafe { str::from_utf8_unchecked(&btop[i_skip..i]) };
            // The value could still be too large for the target integer type.
            let skip: u64 = skip.parse().map_err(|e| {
                E::Search(
                    n_line,
                    format!(
                        "Could not parse {skip:?} as an integer \
                         in btop position {}: {e}",
                        i_skip + 1
                    ),
                )
            })?;
            if skip == 0 {
                err!(("Null value found at btop position {i}."));
            }
            focus += skip;
            if focus >= target {
                // The base of interest in query was matching the subject.
                return Ok((b'=', b'='));
            }
        }};
    }
    // Loop over bytes in the btop sequence.
    for (i, &b) in btop.iter().enumerate() {
        let c = b as char;
        match b {
            b'0'..=b'9' => {
                match state {
                    S::N(_) => {}            // Just scroll until the sequence of digits terminates.
                    S::Z => state = S::N(i), // Start a new sequence of digits.
                    S::S | S::Found { .. } => {
                        err!(
                            ("The query base in btop position {i} \
                              is not followed by a subject nucleotide, but by {c:?}.")
                        );
                    }
                }
            }
            // Only accept/process ascii bytes.
            b if b.is_ascii() => {
                match state {
                    S::N(i_skip) => {
                        if i == 0 {
                            err!(
                                ("Btop sequence starts with misaligned nucleotide \
                                  or non-digit character: {c:?}.")
                            );
                        }
                        process_digits!(i_skip, i);
                    }
                    S::S => {
                        // A subject position we are not interested about.
                        state = S::Z;
                        continue;
                    }
                    S::Z => {} // A contiguous mismatched query position we may be interested about.
                    S::Found(q) => return Ok((q, b)),
                }
                // A mismatched query char is under focus.
                let non_gap = b != b'-';
                focus += u64::from(non_gap);
                state = if non_gap && focus == target {
                    // Found the (mismatching) base of interest.
                    S::Found(b)
                } else {
                    S::S
                };
            }
            _ => {
                err!(("Non-ASCII byte in btop sequence: {c:?} ({b})."));
            }
        }
    }

    if let S::N(i_skip) = state {
        // Parse the last digits to check consistency.
        process_digits!(i_skip, btop.len());
    }
    err!(
        ("Unexpected end of btop sequence \
          before the position of interest was reached ({base_of_interest}).")
    );
}

#[test]
#[allow(clippy::too_many_lines)]
fn short_homologies() {
    let check = |b, btop: &str, exp| {
        println!("Search {btop:?} for position {b}..");
        // Incrementing both qstart and b should not change the outcome.
        let qstart = 1;
        for shift in [0, 5, 10] {
            println!("  (shift by {shift})");
            let (q, s) = search_btop(btop.as_bytes(), b + shift, qstart + shift, 0).unwrap();
            assert_eq!(exp, (q as char, s as char));
        }
    };

    // Basic checks.
    check(4, "4AC5", ('=', '='));
    check(5, "4AC5", ('A', 'C'));
    check(6, "4AC5", ('=', '='));

    // Several mismatched positions.
    check(10, "4AC5TG6", ('=', '='));
    check(11, "4AC5TG6", ('T', 'G'));
    check(12, "4AC5TG6", ('=', '='));

    // Consecutive mismatched positions.
    check(4, "4ACTG6", ('=', '='));
    check(5, "4ACTG6", ('A', 'C'));
    check(6, "4ACTG6", ('T', 'G'));
    check(7, "4ACTG6", ('=', '='));

    // Gaps in subject position.
    check(4, "4A-TG6", ('=', '='));
    check(5, "4A-TG6", ('A', '-'));
    check(6, "4A-TG6", ('T', 'G'));
    check(7, "4A-TG6", ('=', '='));

    check(4, "4ACT-6", ('=', '='));
    check(5, "4ACT-6", ('A', 'C'));
    check(6, "4ACT-6", ('T', '-'));
    check(7, "4ACT-6", ('=', '='));

    check(4, "4A-T-6", ('=', '='));
    check(5, "4A-T-6", ('A', '-'));
    check(6, "4A-T-6", ('T', '-'));
    check(7, "4A-T-6", ('=', '='));

    // Gap in query position (must be skipped).
    check(4, "4-CTG6", ('=', '='));
    check(5, "4-CTG6", ('T', 'G')); // Skipped.
    check(6, "4-CTG6", ('=', '='));
    check(7, "4-CTG6", ('=', '='));

    check(4, "4AC-G6", ('=', '='));
    check(5, "4AC-G6", ('A', 'C'));
    check(6, "4AC-G6", ('=', '=')); // Skipped
    check(7, "4AC-G6", ('=', '='));

    check(4, "4-C-GAT6", ('=', '='));
    check(5, "4-C-GAT6", ('A', 'T')); // Both skipped.
    check(6, "4-C-GAT6", ('=', '='));
    check(7, "4-C-GAT6", ('=', '='));

    // Same with non-contiguous mismatches.
    check(4, "4-C5TG6", ('=', '='));
    check(5, "4-C5TG6", ('=', '='));
    check(6, "4-C5TG6", ('=', '='));
    check(9, "4-C5TG6", ('=', '='));
    check(10, "4-C5TG6", ('T', 'G'));
    check(11, "4-C5TG6", ('=', '='));

    check(4, "4-C5-GAT6", ('=', '='));
    check(5, "4-C5-GAT6", ('=', '='));
    check(6, "4-C5-GAT6", ('=', '='));
    check(9, "4-C5-GAT6", ('=', '='));
    check(10, "4-C5-GAT6", ('A', 'T'));
    check(11, "4-C5-GAT6", ('=', '='));

    let fail = |b, btop: &str, exp| {
        let act = search_btop(btop.as_bytes(), b, 1, 0).expect_err("Unexpected success.");
        if let Error::Search(n, message) = act {
            assert_eq!(n, 0);
            assert_eq!(exp, message);
        } else {
            panic!("Unexpected error type.");
        }
    };

    fail(10, "4ÉC5", "Non-ASCII byte in btop sequence: 'Ã' (195).");
    fail(
        10,
        "4A55",
        "The query base in btop position 2 \
         is not followed by a subject nucleotide, but by '5'.",
    );
    fail(
        10,
        "4ATG55",
        "The query base in btop position 4 \
         is not followed by a subject nucleotide, but by '5'.",
    );
    fail(
        10,
        "A55",
        "Btop sequence starts with misaligned nucleotide or non-digit character: 'A'.",
    );
    fail(10, "4AC0TG", "Null value found at btop position 4.");
    fail(
        10,
        "4AC99999999999999999999TG5",
        "Could not parse \"99999999999999999999\" as an integer \
         in btop position 4: number too large to fit in target type",
    );
    fail(
        10,
        "4A",
        "Unexpected end of btop sequence \
         before the position of interest was reached (10).",
    );
    fail(
        10,
        "4AC2",
        "Unexpected end of btop sequence \
         before the position of interest was reached (10).",
    );
}
