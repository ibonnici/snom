// Retrieve config directly from command line arguments.

use std::collections::HashSet;

use clap::Parser;

use crate::{
    columns::{parse_column_skips, ColumnsSkips, MAX_N_COLS},
    errors::{errout, Error},
};

/// Find out homologous of specific base of a query sequence in blast results.
#[derive(Parser)]
#[clap(author, version, about, long_about = None)]
pub(crate) struct Config {
    /// Input filename with blast results.
    /// It is expected to contain columns information
    /// for ["sequence id", "query start", "query end"], and then
    /// ["btop"] and/or
    /// ["query sequence", "subject sequence"]
    /// in this exact order,
    /// although additional (unused) columns can be inserted in-between.
    /// The exact columns names can be specified as other arguments
    /// if they differ from Blast default columns names.
    /// Use '-' as an input file name to read from standard input instead.
    pub(crate) input: String,

    /// Column separator in input file.
    /// Only single-byte ascii separators are allowed.
    #[clap(display_order = 1)]
    #[clap(short, long, default_value_t = String::from("\t"))]
    input_separator: String,

    /// Column separator in output.
    /// Only single-byte ascii separators are allowed.
    #[clap(display_order = 1)]
    #[clap(short, long, default_value_t = String::from("\t"))]
    output_separator: String,

    /// Column separator in output.
    /// Only single-byte ascii separators are allowed.

    /// Specify which columns to use with this argument.
    /// *Ordering matters* and options are restricted
    /// to avoid backtracking on every line,
    /// which would harm performances.
    /// By default, all relevant columns found in the header will be used.
    /// If no header is provided,
    /// the columns positions need to be provided with --column-positions.
    #[clap(arg_enum, long, display_order = 2)]
    pub(crate) columns_used: Option<ColumnsUsed>,

    /// If no header is provided,
    /// specify column positions in input file, separated with '-',
    /// and according to --columns-used.
    /// *Ordering matters* to avoid backtracking on every line,
    /// which would harm performances.
    /// Example value: "1-9-10-13-14".
    #[clap(long, requires = "columns-used", display_order = 3)]
    pub(crate) columns_positions: Option<String>,

    #[clap(flatten)]
    colnames: ColumnsNames,
}

// Restrict the possible colunms ordering and presence.
#[derive(clap::ArgEnum, Clone, Copy)]
#[allow(clippy::enum_variant_names)]
#[cfg_attr(test, derive(Debug, PartialEq))]
pub(crate) enum ColumnsUsed {
    IdQstartQendBtop,
    IdQstartQendQseqSseq,
    // When both are given (in either order),
    // they are both used and checked for consistency.
    IdQstartQendQseqSseqBtop,
    IdQstartQendBtopQseqSseq,
}

#[derive(Parser)]
pub(crate) struct ColumnsNames {
    /// Name of the column containing the *extended* sequence id,
    /// ie. a string with the following form: <sequence_id>:<nucleotide>:<position>.
    ///
    /// <position> is an integer locating the nucleotide of interest within the query sequence
    /// (first position is 1).
    ///
    /// <nucleotide> is the corresponding base: either 'A', 'T', 'G', 'C'.
    /// This information is optional.
    /// If not given, then it can be deduced from the query match sequence,
    /// unless only the BTOP form is provided and the nucleotide of interest
    /// is a match.
    /// In this situation, the '=' symbol will be output.
    ///
    /// <sequence_id> is ignored by the program.
    #[clap(long, default_value_t = String::from("qseqid"), display_order = 4)]
    pub(crate) id: String,

    /// Name of the column containing query start information:
    /// the start of the match in query sequence coordinates, first is 1.
    #[clap(long, default_value_t = String::from("qstart"), display_order = 5)]
    pub(crate) query_start: String,

    /// Name of the column containing query end information:
    /// the end of the match in query sequence coordinates, first is 1.
    #[clap(long, default_value_t = String::from("qend"), display_order = 6)]
    pub(crate) query_end: String,

    /// Name of the column containing btop strings
    /// summarizing every alignment.
    /// Optional if --query-sequence and --subject-sequence columns are provided.
    #[clap(long, default_value_t = String::from("btop"), display_order = 7)]
    pub(crate) btop: String,

    /// Name of the column containing query sequence information:
    /// an aligned sequence possibly containing gaps,
    /// with the same size as the subject sequence.
    /// Optional if --btop column is provided.
    #[clap(long, default_value_t = String::from("qseq"), display_order = 8)]
    pub(crate) query_sequence: String,

    /// Name of the column containing subject sequence information:
    /// an aligned sequence possibly containing gaps,
    /// with the same size as the query sequence.
    /// Optional if --btop column is provided.
    #[clap(long, default_value_t = String::from("sseq"), display_order = 9)]
    pub(crate) subject_sequence: String,
}

// Parse arguments further to retrieve useful values from them.
impl Config {
    pub(crate) fn separator(&self, input: bool) -> Result<u8, Error> {
        let output = if input { "Input" } else { "Output" };
        let separator = unescape(if input {
            &self.input_separator
        } else {
            &self.output_separator
        });
        if separator.is_empty() {
            errout!(Arguments(format!(
                "{output} column separator cannot be empty."
            )));
        }
        if separator.len() > 1 {
            errout!(Arguments(format!(
                "{output} column separator longer than 1 byte long: {separator:?}."
            )));
        }
        Ok(separator.as_bytes()[0])
    }

    // Check colum names provided by user before retrieving them.
    pub(crate) fn column_names(&self) -> Result<&ColumnsNames, Error> {
        // Check that all names do differ at least.
        let mut set = HashSet::with_capacity(MAX_N_COLS);
        for name in self.colnames.iter() {
            if set.contains(name) {
                errout!(Arguments(format!(
                    "Cannot discriminate two columns with the same name in header: {name:?}."
                )));
            }
            set.insert(name);
        }
        Ok(&self.colnames)
    }

    // Translate columns positions into the relevant skips.
    pub(crate) fn column_skips(&self, used: ColumnsUsed) -> Option<Result<ColumnsSkips, Error>> {
        self.columns_positions
            .as_ref()
            .map(|input| parse_column_skips(input.as_str(), used))
    }
}

// Basic unescaping for 'separator' string.
pub(crate) fn unescape(input: &str) -> String {
    let mut result = String::from(input);
    for (a, b) in &[(r"\n", "\n"), (r"\t", "\t"), (r"\\", "\\")] {
        result = result.replace(a, b);
    }
    result
}

// Useful for testing.
#[cfg(test)]
impl Default for ColumnsNames {
    fn default() -> Self {
        macro_rules! defaults {
            ($($field:ident: $name:literal),+$(,)?) => {
                Self {
                    $( $field: $name.into() ),+
                }
            };
        }
        defaults! {
            id: "id",
            query_start: "qstart",
            query_end: "qend",
            btop: "btop",
            query_sequence: "qseq",
            subject_sequence: "sseq",
        }
    }
}
