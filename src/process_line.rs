// Assuming we know all the required context value,
// process one line in the input file.
// Performance matters here, because this happens on every line
// as the reader scans forward through the file.
// The line is assumed to be ascii, but if it's not, then utf-8 is not enforced.

use ascii::AsciiStr;

use crate::{
    columns::{AlignmentSkips, ColumnsSkips},
    config::ColumnsNames,
    errors::{errout, Error},
    search_btop::search_btop,
    search_match_sequences::search_match_sequences,
};

// Return the focal nucleotide (query)
// along with the aligned character in the subject.
// '.' is returned in case the focal nucleotide is not aligned.
pub(crate) fn process_line(
    line: &[u8],
    n_line: usize,
    colskips: &ColumnsSkips,
    separator: u8,
    colnames: &ColumnsNames,
) -> Result<(u8, u8), Error> {
    use AlignmentSkips as A;
    use Error as E;

    macro_rules! err {
        ($type:ident $fmt:tt) => {
            E::$type(n_line, format!$fmt)
        };
        ($fmt:tt) => {
            E::LineParse(n_line, format!$fmt) // Common default in this file.
        };
        (:out $args:tt) => {
            return Err(err!$args);
        };
    }

    // Read fields according to the pre-calculated skips.
    let mut fields = line.split(|&b| b == separator);
    macro_rules! next {
        ($skip:expr, $name:expr) => {{
            let name = $name;
            let sep = separator as char;
            fields
                .nth($skip)
                .ok_or_else(|| err!(("Could not find column {name:?} with separator {sep:?}.")))?
        }};
        (int $skip:expr, $name:expr) => {{
            let name = $name;
            let field = next!($skip, name);
            let field = AsciiStr::from_ascii(field)
                .map_err(|e| err!(("Non-ascii character in numerical field {name:?}: {e}")))?;
            field.as_str().parse().map_err(|e| {
                err!(("Could not read field {name:?} ({field:?}) as an integer: {e}."))
            })?
        }};
    }
    let seqid = next!(colskips.id, &colnames.id);
    let qstart = next!(int colskips.query_start, &colnames.query_start);
    let qend = next!(int colskips.query_end, &colnames.query_end);

    // Extract base position from the sequence id.
    let mut parts = seqid.rsplitn(2, |&b| b == b':');
    // SAFETY: the last part is at worst empty.
    let b = unsafe { parts.next().unwrap_unchecked() };
    let seqid = parts.next().ok_or_else(|| {
        err!((
            "Could not find ':' separator at the end of field {:?} \
             to read nucleotide position in query sequence.",
            colnames.id
        ))
    })?;
    let b = AsciiStr::from_ascii(b)
        .map_err(|e| err!(("Non-ascii character in nucleotide position: {e}")))?
        .as_str();
    let base_position: u64 = b
        .parse()
        .map_err(|e| err!(("Could not read {b:?} as a nucleotide position: {e}")))?;
    // Humans count from 1.
    for (i, name) in [
        (qstart, "query start"),
        (qend, "query end"),
        (base_position, "base"),
    ] {
        if i == 0 {
            err!(:out (Consistency(
                "The {name} position is found to be zero. But the minimum position is 1."
            )));
        }
    }

    // Extract optional nucleotide information.
    let mut parts = seqid.rsplitn(2, |&b| b == b':');
    // SAFETY: the last part is at worst empty.
    let n = unsafe { parts.next().unwrap_unchecked() };
    let nucleotide = (n.len() == 1)
        .then(|| {
            let n = n[0];
            n.is_ascii().then_some(n)
        })
        .flatten();

    // Check that the base of interest is actually part of the current blast result.
    if qend < base_position || base_position < qstart {
        // Otherwise output missing data and move on.
        return Ok((b'.', b'.'));
    }

    // Tolerate this branch on every line for now, because it is very predictable.
    // TODO: get rid of it at the cost of heavy monomorphization?
    match colskips.alignment {
        A::Btop([btop]) => {
            let btop = next!(btop, &colnames.btop);
            process_btop(qstart, base_position, nucleotide, btop, n_line)
        }
        A::MatchSequences([q_seq, s_seq]) => {
            let q_seq = next!(q_seq, &colnames.query_start);
            let s_seq = next!(s_seq, &colnames.query_end);
            process_match_sequences(qstart, base_position, nucleotide, q_seq, s_seq, n_line)
        }
        #[rustfmt::skip] // Don't split args.
        A::BtopThenSequences([btop, q_seq, s_seq]) => {
            let btop = next!(btop, &colnames.btop);
            let q_seq = next!(q_seq, &colnames.query_start);
            let s_seq = next!(s_seq, &colnames.query_end);
            process_both(qstart, base_position, nucleotide, btop, q_seq, s_seq, n_line)
        }
        #[rustfmt::skip] // Don't split args.
        A::SequencesThenBtop([q_seq, s_seq, btop]) => {
            let q_seq = next!(q_seq, &colnames.query_start);
            let s_seq = next!(s_seq, &colnames.query_end);
            let btop = next!(btop, &colnames.btop);
            process_both(qstart, base_position, nucleotide, btop, q_seq, s_seq, n_line)
        }
    }
}

fn process_btop(
    qstart: u64,
    base_position: u64,
    nucleotide: Option<u8>,
    btop: &[u8],
    n_line: usize,
) -> Result<(u8, u8), Error> {
    if btop.is_empty() {
        errout!(LineParse(n_line, "Btop sequence is empty.".into()));
    }
    let (q, s) = search_btop(btop, base_position, qstart, n_line)?;

    // If one query nucleotide had been hinted,
    // use it to resolve possible '=' symbols or check consistency.
    match (nucleotide, q) {
        (Some(n), b'=') => Ok((n, n)),
        (Some(n), q) => {
            if n == q {
                Ok((q, s))
            } else {
                let (q, n) = (q as char, n as char);
                errout!(Consistency(
                    n_line,
                    format!(
                        "The nucleotide in position {base_position} of the btop sequence \
                         (knowing that the match starts at position {qstart}) \
                         is not {n:?} as expected, but {q:?}."
                    )
                ));
            }
        }
        (None, _) => Ok((q, s)),
    }
}

fn process_match_sequences(
    qstart: u64,
    base_position: u64,
    nucleotide: Option<u8>,
    query: &[u8],
    subject: &[u8],
    n_line: usize,
) -> Result<(u8, u8), Error> {
    macro_rules! err {
        ($args:tt) => {
            errout!(Consistency(n_line, format!$args));
        };
    }

    // Consistency checks.
    let (ql, sl) = (query.len(), subject.len());
    for (l, name) in [(ql, "Query"), (sl, "Subject")] {
        if l == 0 {
            errout!(LineParse(n_line, format!("{name} sequence is empty.")));
        }
    }
    if ql != sl {
        err!(("Query sequence size ({ql}) and subject sequence size ({sl}) do not match."));
    }
    let [q, s] = [query, subject].map(|s| s[0] == b'-');
    if q || s {
        let sequence = if q { "Query" } else { "Subject" };
        err!(("{sequence} sequence starts with a gap."));
    }

    let (q, s) = search_match_sequences(query, subject, base_position, qstart, n_line)?;

    // If one query nucleotide had been hinted,
    // check that it matches otherwise something must be wrong upstream.
    if let Some(n) = nucleotide {
        if q != n {
            let (q, n) = (q as char, n as char);
            err!(
                ("The nucleotide in position {base_position} of the query sequence \
                  (knowing that the match starts at position {qstart}) \
                  is not {n:?} as expected, but {q:?}.")
            );
        }
    }

    Ok((q, s))
}

fn process_both(
    qstart: u64,
    base_position: u64,
    nucleotide: Option<u8>,
    btop: &[u8],
    query: &[u8],
    subject: &[u8],
    n_line: usize,
) -> Result<(u8, u8), Error> {
    let (q, s) = process_btop(qstart, base_position, nucleotide, btop, n_line)?;
    let btop = (q as char, s as char);
    let (q, s) =
        process_match_sequences(qstart, base_position, nucleotide, query, subject, n_line)?;
    let seqs = (q as char, s as char);
    if btop != seqs {
        errout!(Consistency(
            n_line,
            format!(
                "The alignment result according to the btop information is {btop:?}, \
                 but it is {seqs:?} according to sequences informations."
            )
        ));
    }
    Ok((q, s))
}

#[cfg(test)]
mod tests {
    use std::str;

    use super::*;
    use crate::{columns::parse_column_skips, config::ColumnsUsed};

    #[test]
    #[allow(clippy::too_many_lines)]
    fn both_approaches() {
        use Error as E;

        println!("Expecting successes:");
        let colnames = ColumnsNames::default();
        let skips =
            parse_column_skips("1-2-3-5-6-7", ColumnsUsed::IdQstartQendQseqSseqBtop).unwrap();
        let success = |line: &str, exp| {
            println!("  {line:?}");
            let (q, s) = process_line(line.as_bytes(), 1, &skips, b' ', &colnames).unwrap();
            assert_eq!(exp, (q as char, s as char));
        };

        // With nucleotide hint.
        success("id:T:14 11 17 :: AA-ATCCC AANAG-CC 2-N1TGC-2", ('T', 'G'));
        // Without.
        success("id:14 11 17 :: AA-ATCCC AANAG-CC 2-N1TGC-2", ('T', 'G'));
        success("id::14 11 17 :: AA-ATCCC AANAG-CC 2-N1TGC-2", ('T', 'G'));
        // Not aligned.
        success("id:T:14 11 13 :: AA-A AANA 2-N1", ('.', '.'));
        // Ignored unused columns.
        success(
            "id:T:14 11 17 Z̤͊a͓̫l͎̀ġ̞o͋͠ AA-ATCCC AANAG-CC 2-N1TGC-2",
            ('T', 'G'),
        );
        success("id:T:14 11 17  AA-ATCCC AANAG-CC 2-N1TGC-2", ('T', 'G')); // (unused is empty)
                                                                           // Ignore trailing input
        success(
            "id:T:14 11 17  AA-ATCCC AANAG-CC 2-N1TGC-2 EXTRA FIELDS",
            ('T', 'G'),
        );
        success("id:T:14 11 17  AA-ATCCC AANAG-CC 2-N1TGC-2 ", ('T', 'G'));
        success("id:T:14 11 17  AA-ATCCC AANAG-CC 2-N1TGC-2\r", ('T', 'G')); // (this in particular)

        println!("Expecting failures:");
        macro_rules! fail {
            ($line:literal => $type:ident($mess:literal)) => {{
                let line = $line;
                println!("  {line:?}");
                let act = process_line(line.as_bytes(), 1, &skips, b' ', &colnames).unwrap_err();
                if let E::$type(1, act) = act {
                    assert_eq!($mess, act);
                } else {
                    let typ = stringify!($type);
                    panic!("Invalid error type. Expected {typ}, got {act:?}");
                }
            }};
        }

        // Missing fields.
        fail!("11 17 :: AA-ATCCC AANAG-CC 2-N1TGC-2" =>
            LineParse("Could not read field \"qend\" (\"::\") as an integer: \
                       invalid digit found in string.")
        );
        fail!("id:T:14 17 :: AA-ATCCC AANAG-CC 2-N1TGC-2" =>
            LineParse("Could not read field \"qend\" (\"::\") as an integer: \
                       invalid digit found in string.")
        );
        fail!("id:T:14 11 17 AA-ATCCC AANAG-CC 2-N1TGC-2" =>
            LineParse("Could not find column \"btop\" with separator ' '.")
        );
        fail!("id:T:14 11 17 :: AANAG-CC 2-N1TGC-2" =>
            LineParse("Could not find column \"btop\" with separator ' '.")
        );

        // Empty fields.
        fail!(" 11 17 :: AA-ATCCC AANAG-CC 2-N1TGC-2" =>
            LineParse("Could not find ':' separator at the end of field \"id\" \
                       to read nucleotide position in query sequence.")
        );
        fail!("id:T:14  17 :: AA-ATCCC AANAG-CC 2-N1TGC-2" =>
            LineParse("Could not read field \"qstart\" (\"\") as an integer: \
                       cannot parse integer from empty string.")
        );
        fail!("id:T:14 11  :: AA-ATCCC AANAG-CC 2-N1TGC-2" =>
            LineParse("Could not read field \"qend\" (\"\") as an integer: \
                       cannot parse integer from empty string.")
        );
        fail!("id:T:14 11 17 ::  AANAG-CC 2-N1TGC-2" =>
             LineParse("Query sequence is empty.")
        );
        fail!("id:T:14 11 17 :: AA-ATCCC  2-N1TGC-2" =>
             LineParse("Subject sequence is empty.")
        );
        fail!("id:T:14 11 17 :: AA-ATCCC AANAG-CC " =>
             LineParse("Btop sequence is empty.")
        );

        // Integer parse.
        fail!("id:T:1a 11 17 :: AA-ATCCC AANAG-CC 2-N1TGC-2" =>
            LineParse("Could not read \"1a\" as a nucleotide position: \
                       invalid digit found in string")
        );
        fail!("id:T:1à 11 17 :: AA-ATCCC AANAG-CC 2-N1TGC-2" =>
            LineParse("Non-ascii character in nucleotide position: \
                       the byte at index 1 is not ASCII")
        );
        fail!("id:T:0 11 17 :: AA-ATCCC AANAG-CC 2-N1TGC-2" =>
            Consistency("The base position is found to be zero. But the minimum position is 1.")
        );
        fail!("id:T:14 1a 17 :: AA-ATCCC AANAG-CC 2-N1TGC-2" =>
            LineParse("Could not read field \"qstart\" (\"1a\") as an integer: \
                       invalid digit found in string.")
        );
        fail!("id:T:14 1à 17 :: AA-ATCCC AANAG-CC 2-N1TGC-2" =>
            LineParse("Non-ascii character in numerical field \"qstart\": \
                       the byte at index 1 is not ASCII")
        );
        fail!("id:T:14 00 17 :: AA-ATCCC AANAG-CC 2-N1TGC-2" =>
            Consistency("The query start position is found to be zero. \
                         But the minimum position is 1.")
        );

        // Missing nucleotide position.
        fail!("xy 11 17 :: AA-ATCCC AANAG-CC 2-N1TGC-2" =>
            LineParse("Could not find ':' separator at the end of field \"id\" \
                       to read nucleotide position in query sequence.")
        );
        fail!("id: 11 17 :: AA-ATCCC AANAG-CC 2-N1TGC-2" =>
            LineParse("Could not read \"\" as a nucleotide position: \
                       cannot parse integer from empty string")
        );

        // Inconsistent fields.
        fail!("id:T:14 11 17 :: AA-ATCC AANAG-CC 2-N1TGC-2" =>
             Consistency("Query sequence size (7) and subject sequence size (8) do not match.")
        );
        fail!("id:X:14 11 17 :: AA-ATCCC AANAG-CC 2-N1TGC-2" =>
             Consistency("The nucleotide in position 14 of the btop sequence \
                          (knowing that the match starts at position 11) \
                          is not 'X' as expected, but 'T'.")
        );
        fail!("id:X:14 11 17 :: AA-ATCCC AANAG-CC 2-N1XGC-2" =>
             Consistency("The nucleotide in position 14 of the query sequence \
                          (knowing that the match starts at position 11) \
                          is not 'X' as expected, but 'T'.")
        );
        fail!("id:T:14 11 17 :: AA-A AANA 2-N1TGC-2" =>
             Search("Query sequence shorter than expected.")
        );
        fail!("id:T:14 11 17 :: AA-ATCCC AANAG-CC 2-N1" =>
             Search("Unexpected end of btop sequence \
                     before the position of interest was reached (14).")
        );
        fail!("id:T:14 11 17 :: -A-ATCCC AANAG-CC 2-N1TGC-2" =>
             Consistency("Query sequence starts with a gap.")
        );
        fail!("id:T:14 11 17 :: AA-ATCCC -ANAG-CC 2-N1TGC-2" =>
             Consistency("Subject sequence starts with a gap.")
        );
        fail!("id:T:14 11 17 :: AA-ATCCC AANAG-CC 2-N1TXC-2" =>
             Consistency("The alignment result according to the btop information is ('T', 'X'), \
                          but it is ('T', 'G') according to sequences informations.")
        );
    }

    #[test]
    fn btop_only() {
        let colnames = ColumnsNames::default();
        let skips = parse_column_skips("1-2-3-5", ColumnsUsed::IdQstartQendBtop).unwrap();
        let success = |line: &str, exp| {
            let (q, s) = process_line(line.as_bytes(), 1, &skips, b' ', &colnames).unwrap();
            assert_eq!(exp, (q as char, s as char));
        };

        // With nucleotide hint.
        success("id:T:14 11 17 :: 2-N1TGC-2", ('T', 'G'));
        success("id:T:14 11 17 :: 5C-2", ('T', 'T')); // Hint used.

        // Without.
        success("id:14 11 17 :: 2-N1TGC-2", ('T', 'G'));
        success("id:14 11 17 :: 5C-2", ('=', '=')); // Cannot tell which nucleotide, but it's aligned.
    }
}
