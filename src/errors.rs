// The various errors possibly encountered during the process
// and how they render.

use std::io::Error as IOError;

use ansi_term::Colour;

macro_rules! errout {
    ($variant:ident $args:tt) => {
        return Err(crate::errors::Error::$variant$args);
    };
}
pub(crate) use errout;

#[cfg_attr(test, derive(Debug))]
pub(crate) enum Error {
    Arguments(String),
    InputIO(IOError),
    HeaderParse(String),
    // The following few ones carry line number.
    InputLineIO(usize, IOError),
    LineParse(usize, String),
    Consistency(usize, String),
    Search(usize, String),
    OutputIO(IOError),
}

impl Error {
    pub(crate) fn write_header(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use Error as E;
        f.write_str(match self {
            E::Arguments { .. } => "Error in arguments",
            E::InputIO { .. } | E::InputLineIO(..) => "Input error",
            E::HeaderParse { .. } => "Error when parsing header",
            E::LineParse { .. } => "Error when parsing line",
            E::Consistency { .. } => "Inconsistent input",
            E::Search { .. } => "Search error",
            E::OutputIO { .. } => "Output error",
        })
    }

    pub(crate) fn write_message(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use Error as E;
        match self {
            E::InputIO(e) | E::OutputIO(e) => write!(f, "{e}"),
            E::InputLineIO(i, e) => write!(f, "Line {i}: {e}"),
            E::LineParse(i, e) | E::Consistency(i, e) | E::Search(i, e) => {
                write!(f, "Line {i}: {e}")
            }
            E::Arguments(e) | E::HeaderParse(e) => write!(f, "{e}"),
        }
    }

    pub(crate) fn code(&self) -> i32 {
        use Error as E;
        match self {
            E::Arguments { .. } => 1,
            E::InputIO { .. } => 2,
            E::HeaderParse { .. } => 3,
            E::InputLineIO { .. } => 4,
            E::LineParse { .. } => 5,
            E::Consistency { .. } => 6,
            E::Search { .. } => 7,
            E::OutputIO { .. } => 8,
        }
    }
}

use std::fmt::{self, Display};
impl Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let style = Colour::Red.bold();
        write!(f, "{}", style.prefix())?;
        self.write_header(f)?;
        write!(f, ":{} ", style.suffix())?;
        self.write_message(f)
    }
}
